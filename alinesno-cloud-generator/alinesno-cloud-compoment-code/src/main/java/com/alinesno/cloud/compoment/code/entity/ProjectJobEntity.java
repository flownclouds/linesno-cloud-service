package com.alinesno.cloud.compoment.code.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 任务
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Entity
@Table(name="project_job")
public class ProjectJobEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 项目编码
     */
	private String projectCode;
    /**
     * 任务编码
     */
	private String code;
    /**
     * 第多少次执行
     */
	private String number;
    /**
     * 任务状态: 创建[Create] , 执行中[Executing], 完成[C ompleted] ,失败[Error] 警告 [Waring]
     */
	private String state;
    /**
     * 执行任务时间
     */
	private Date createTime;


	public String getProjectCode() {
		return projectCode;
	}

	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}


	@Override
	public String toString() {
		return "ProjectJobEntity{" +
			"projectCode=" + projectCode +
			", code=" + code +
			", number=" + number +
			", state=" + state +
			", createTime=" + createTime +
			"}";
	}
}
