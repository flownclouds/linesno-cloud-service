package com.alinesno.cloud.compoment.code.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 字段属性映射信息
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Entity
@Table(name="map_field_column")
public class MapFieldColumnEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 映射编码
     */
	private String mapClassTableCode;
    /**
     * 字段属性映射编码
     */
	private String code;
    /**
     * 字段
     */
	private String column;
    /**
     * 属性
     */
	private String field;
    /**
     * 字段类型
     */
	private String sqlType;
    /**
     * 属性类型
     */
	private String fieldType;
    /**
     * 注释
     */
	private String notes;
    /**
     * 字段默认值
     */
	private String defaultValue;
    /**
     * 是否是主键
     */
	private String isPrimaryKey;
    /**
     * 是否是时间类型
     */
	private String isDate;
    /**
     * 是否是状态
     */
	private String isState;


	public String getMapClassTableCode() {
		return mapClassTableCode;
	}

	public void setMapClassTableCode(String mapClassTableCode) {
		this.mapClassTableCode = mapClassTableCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getSqlType() {
		return sqlType;
	}

	public void setSqlType(String sqlType) {
		this.sqlType = sqlType;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getIsPrimaryKey() {
		return isPrimaryKey;
	}

	public void setIsPrimaryKey(String isPrimaryKey) {
		this.isPrimaryKey = isPrimaryKey;
	}

	public String getIsDate() {
		return isDate;
	}

	public void setIsDate(String isDate) {
		this.isDate = isDate;
	}

	public String getIsState() {
		return isState;
	}

	public void setIsState(String isState) {
		this.isState = isState;
	}


	@Override
	public String toString() {
		return "MapFieldColumnEntity{" +
			"mapClassTableCode=" + mapClassTableCode +
			", code=" + code +
			", column=" + column +
			", field=" + field +
			", sqlType=" + sqlType +
			", fieldType=" + fieldType +
			", notes=" + notes +
			", defaultValue=" + defaultValue +
			", isPrimaryKey=" + isPrimaryKey +
			", isDate=" + isDate +
			", isState=" + isState +
			"}";
	}
}
