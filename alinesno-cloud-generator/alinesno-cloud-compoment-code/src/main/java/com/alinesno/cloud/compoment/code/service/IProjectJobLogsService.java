package com.alinesno.cloud.compoment.code.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.common.core.services.IBaseService;
import com.alinesno.cloud.compoment.code.entity.ProjectJobLogsEntity;
import com.alinesno.cloud.compoment.code.repository.ProjectJobLogsRepository;

/**
 * <p> 任务日志 服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@NoRepositoryBean
public interface IProjectJobLogsService extends IBaseService<ProjectJobLogsRepository, ProjectJobLogsEntity, String> {

}
