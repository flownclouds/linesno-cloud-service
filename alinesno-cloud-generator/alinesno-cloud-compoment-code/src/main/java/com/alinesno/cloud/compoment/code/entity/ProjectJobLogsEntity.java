package com.alinesno.cloud.compoment.code.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 任务日志
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Entity
@Table(name="project_job_logs")
public class ProjectJobLogsEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 任务编码
     */
	private String code;
    /**
     * 日志
     */
	private String log;


	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}


	@Override
	public String toString() {
		return "ProjectJobLogsEntity{" +
			"code=" + code +
			", log=" + log +
			"}";
	}
}
