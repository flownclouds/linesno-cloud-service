package com.alinesno.cloud.base.notice.common;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alinesno.cloud.base.notice.entity.EmailErrorEntity;
import com.alinesno.cloud.base.notice.entity.EmailHistoryEntity;
import com.alinesno.cloud.base.notice.entity.EmailSendEntity;
import com.alinesno.cloud.base.notice.service.IEmailErrorService;
import com.alinesno.cloud.base.notice.service.IEmailHistoryService;

/**
 * 邮件发送组件
 * @author LuoAnDong
 * @since 2019年3月24日 下午9:10:59
 */
@Component
public class EmailComponent {
	
	@Autowired
	private IEmailErrorService emailErrorService ; 
	
	@Autowired
	private IEmailHistoryService emailHistoryService ; 
	
	/**
	 * 记录错误邮件
	 */
	public void saveError(EmailSendEntity email) {
		
		EmailErrorEntity e = new EmailErrorEntity() ; 
		BeanUtils.copyProperties(email, e);
		
		emailErrorService.save(e) ; 
	}
	
	/**
	 * 记录邮件历史 
	 */
	public void saveHistory(EmailSendEntity email) {
		
		EmailHistoryEntity e = new EmailHistoryEntity() ; 
		BeanUtils.copyProperties(email, e);
		
		emailHistoryService.save(e) ; 
	}
	
}
