package com.alinesno.cloud.base.boot.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.alinesno.cloud.base.boot.enums.MenuEnums;
import com.alinesno.cloud.common.core.orm.entity.BaseEntity;

/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Entity
@Table(name = "manager_resource")
public class ManagerResourceEntity extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * 资源名称
	 */
	@Column(name = "resource_name")
	private String resourceName;
	/**
	 * 资源链接
	 */
	@Column(name = "resource_link")
	private String resourceLink;
	/**
	 * 资源图标
	 */
	@Column(name = "resource_icon")
	private String resourceIcon;
	/**
	 * 资源父类
	 */
	@Column(name = "resource_parent")
	private String resourceParent = "0" ;
	/**
	 * 资源排序
	 */
	@Column(name = "resource_order")
	private Integer resourceOrder;

	/**
	 * 菜单类型(0菜单|1小标题|9平台标题)
	 */
	@Column(name = "menu_type")
	private String menuType = MenuEnums.MENU_ITEM.value ;

	/**
	 * 菜单子类
	 */
	@Transient
	private List<ManagerResourceEntity> subResource ; 

	public List<ManagerResourceEntity> getSubResource() {
		return subResource;
	}

	public void setSubResource(List<ManagerResourceEntity> subResource) {
		this.subResource = subResource;
	}

	public String getMenuType() {
		return menuType;
	}

	public void setMenuType(String menuType) {
		this.menuType = menuType;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public String getResourceLink() {
		return resourceLink;
	}

	public void setResourceLink(String resourceLink) {
		this.resourceLink = resourceLink;
	}

	public String getResourceIcon() {
		return resourceIcon;
	}

	public void setResourceIcon(String resourceIcon) {
		this.resourceIcon = resourceIcon;
	}

	public String getResourceParent() {
		return resourceParent;
	}

	public void setResourceParent(String resourceParent) {
		this.resourceParent = resourceParent;
	}

	public Integer getResourceOrder() {
		return resourceOrder;
	}

	public void setResourceOrder(Integer resourceOrder) {
		this.resourceOrder = resourceOrder;
	}

	@Override
	public String toString() {
		return "ManagerResourceEntity{" + "resourceName=" + resourceName + ", resourceLink=" + resourceLink
				+ ", resourceIcon=" + resourceIcon + ", resourceParent=" + resourceParent + ", resourceOrder="
				+ resourceOrder + "}";
	}
}
