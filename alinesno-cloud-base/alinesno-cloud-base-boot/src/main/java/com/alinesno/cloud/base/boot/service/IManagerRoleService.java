package com.alinesno.cloud.base.boot.service;

import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.base.boot.entity.ManagerAccountEntity;
import com.alinesno.cloud.base.boot.entity.ManagerRoleEntity;
import com.alinesno.cloud.base.boot.repository.ManagerRoleRepository;
import com.alinesno.cloud.common.core.services.IBaseService;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@NoRepositoryBean
public interface IManagerRoleService extends IBaseService<ManagerRoleRepository, ManagerRoleEntity, String> {

	/**
	 * 保存用户权限
	 * @param managerRoleEntity
	 * @param functiondIds
	 * @return
	 */
	boolean saveRole(ManagerRoleEntity managerRoleEntity, String functionIds);

	/**
	 * 保存用户角色 
	 * @param accountEntity
	 * @param rolesId
	 * @return
	 */
	boolean authAccount(ManagerAccountEntity accountEntity, String rolesId);

	/**
	 * 查询用户所有角色
	 * @param accountId
	 * @return
	 */
	List<ManagerRoleEntity> findByAccountId(String accountId);

}
