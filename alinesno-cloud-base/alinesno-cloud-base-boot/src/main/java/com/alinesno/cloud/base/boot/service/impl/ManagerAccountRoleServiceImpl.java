package com.alinesno.cloud.base.boot.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.base.boot.entity.ManagerAccountRoleEntity;
import com.alinesno.cloud.base.boot.repository.ManagerAccountRoleRepository;
import com.alinesno.cloud.base.boot.service.IManagerAccountRoleService;
import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-08 08:14:52
 */
@Service
public class ManagerAccountRoleServiceImpl extends IBaseServiceImpl<ManagerAccountRoleRepository, ManagerAccountRoleEntity, String> implements IManagerAccountRoleService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ManagerAccountRoleServiceImpl.class);

	@Override
	public List<ManagerAccountRoleEntity> findAllByAccountId(String accountId) {
		return jpa.findAllByAccountId(accountId);
	}

	@Override
	public void deleteByAccountId(String accountId) {
		jpa.deleteByAccountId(accountId);
	}

}
