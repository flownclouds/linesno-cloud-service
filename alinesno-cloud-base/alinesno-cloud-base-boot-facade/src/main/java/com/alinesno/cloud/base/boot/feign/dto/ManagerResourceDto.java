package com.alinesno.cloud.base.boot.feign.dto;

import java.util.List;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:00:40
 */
@SuppressWarnings("serial")
public class ManagerResourceDto extends BaseDto {

    /**
     * 资源名称
     */
	private String resourceName;
	
    /**
     * 资源链接
     */
	private String resourceLink;
	
    /**
     * 资源图标
     */
	private String resourceIcon;
	
    /**
     * 资源父类
     */
	private String resourceParent;
	
    /**
     * 资源排序
     */
	private Integer resourceOrder;
	
	/**
	 * 菜单类型(0菜单|1小标题|9平台标题)
	 */
	private String menuType ; 
	
	/**
	 * 菜单子类
	 */
	private List<ManagerResourceDto> subResource ; 

	public List<ManagerResourceDto> getSubResource() {
		return subResource;
	}

	public void setSubResource(List<ManagerResourceDto> subResource) {
		this.subResource = subResource;
	}
	
	public String getMenuType() {
		return menuType;
	}

	public void setMenuType(String menuType) {
		this.menuType = menuType;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public String getResourceLink() {
		return resourceLink;
	}

	public void setResourceLink(String resourceLink) {
		this.resourceLink = resourceLink;
	}

	public String getResourceIcon() {
		return resourceIcon;
	}

	public void setResourceIcon(String resourceIcon) {
		this.resourceIcon = resourceIcon;
	}

	public String getResourceParent() {
		return resourceParent;
	}

	public void setResourceParent(String resourceParent) {
		this.resourceParent = resourceParent;
	}

	public Integer getResourceOrder() {
		return resourceOrder;
	}

	public void setResourceOrder(Integer resourceOrder) {
		this.resourceOrder = resourceOrder;
	}

}
