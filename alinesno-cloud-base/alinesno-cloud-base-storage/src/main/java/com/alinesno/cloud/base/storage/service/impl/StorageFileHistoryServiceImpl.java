package com.alinesno.cloud.base.storage.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.base.storage.entity.StorageFileHistoryEntity;
import com.alinesno.cloud.base.storage.repository.StorageFileHistoryRepository;
import com.alinesno.cloud.base.storage.service.IStorageFileHistoryService;
import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-09 20:22:17
 */
@Service
public class StorageFileHistoryServiceImpl extends IBaseServiceImpl<StorageFileHistoryRepository, StorageFileHistoryEntity, String> implements IStorageFileHistoryService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(StorageFileHistoryServiceImpl.class);

}
