package com.alinesno.cloud.base.storage.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 七牛文件配置属性
 * @author LuoAnDong
 * @since 2019年4月10日 上午6:16:19
 */
@Component
@ConfigurationProperties(prefix = "storage.local")
public class StorageLocalConfig {
	
	private String savePath ;

	public String getSavePath() {
		return savePath;
	}

	public void setSavePath(String savePath) {
		this.savePath = savePath;
	} 
   
}
