package com.alinesno.cloud.base.logger.repository;

import com.alinesno.cloud.base.logger.entity.LogRecordEntity;
import com.alinesno.cloud.common.core.orm.repository.IBaseJpaRepository;

/**
 * <p>
  *  持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:16:06
 */
public interface LogRecordRepository extends IBaseJpaRepository<LogRecordEntity, String> {

}
