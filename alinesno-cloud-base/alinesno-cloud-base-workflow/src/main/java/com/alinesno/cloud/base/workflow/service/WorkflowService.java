package com.alinesno.cloud.base.workflow.service;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import com.alinesno.cloud.base.workflow.bean.ActivityDefineEntity;
import com.alinesno.cloud.base.workflow.bean.ActivityInstEntity;
import com.alinesno.cloud.base.workflow.bean.ApproveBean;
import com.alinesno.cloud.base.workflow.bean.BusinessBean;
import com.alinesno.cloud.base.workflow.bean.BusinessTransactionBean;
import com.alinesno.cloud.base.workflow.bean.WorkItemBean;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;

/**
 * 工作流业务服务
 * 
 * @author LuoAnDong
 * @since 2019年4月17日 下午6:09:34
 */
public interface WorkflowService {
	/**
	 * 【测试示例】业务上报
	 * 
	 * @param sbVo
	 * @return
	 */
	ResponseEntity<?> confirmBiz(ApproveBean sbVo);

	/**
	 * 【测试示例】业务审批
	 * 
	 * @param spVo
	 * @return
	 */
	ResponseEntity<?> approveBiz(ApproveBean spVo);

	/**
	 * 【测试示例】查询业务待办信息列表
	 * 
	 * @param wrapper
	 *            业务查询条件
	 * @param userId
	 *            用户ID
	 * @return
	 */
	List<BusinessBean> searchTodoTasks(RestWrapper wrapper, String userId);

	/**
	 * 【测试示例】查询业务待办信息列表
	 * 
	 * @param wrapper
	 *            业务查询条件
	 * @param page
	 *            分页参数
	 * @param userId
	 *            用户ID
	 * @return
	 */
	Page<BusinessBean> searchTodoTaskForPage(RestWrapper wrapper, Page<BusinessBean> page, String userId);

	/**
	 * 【调用集成】待办查询条件封装
	 * 
	 * @param processDefName
	 *            流程定义名称
	 * @param userId
	 *            用户ID
	 * @param lcjds
	 *            流程节点列表， 可插入空
	 * @return IN (bpsid1, bpsid2, bpsid3, ......)
	 */
	String getTodoTaskIns(String processDefName, String userId, String... lcjds);

	/**
	 * 【调用集成】获取待办事工作项列表
	 * 
	 * @param processDefName
	 *            流程定义名称
	 * @param userId
	 *            用户ID
	 * @param lcjds
	 *            流程节点列表， 可为空
	 * @return
	 */
	List<WorkItemBean> getTodoTasks(String processDefName, String userId, String... lcjds);

	/**
	 * 根据当前实例id，活动定义ID获取下一分支节点活动ID列表
	 * 
	 * @param processDefId
	 *            流程实例ID
	 * @param activityDefId
	 *            活动定义ID
	 * @return
	 */
	List<String> getNextNodes(long processDefId, String activityDefId);

	/**
	 * 根据当前实例id，活动定义ID获取下一分支节点列表
	 * 
	 * @param processDefId
	 *            流程实例ID
	 * @param activityDefId
	 *            活动定义ID
	 * @return
	 */
	List<ActivityDefineEntity> getNextActNodes(long processDefId, String activityDefId);

	/**
	 * 根据根流程实例获取当前节点的直接后继活动定义
	 * <P>
	 * 当存在子流程时使用该方法
	 * </P>
	 * 如果当前工作项所在的节点的后续活动是结束节点，并且该工作项是属于子流程的工作项，则获取父流程实例的当前节点的后续活动定义
	 * 
	 * @param rootProcessInstID
	 *            根流程实例ID
	 */
	List<ActivityDefineEntity> getNextActNodes(long rootProcessInstID);

	/**
	 * 【调用集成】启动流程
	 * 
	 * @param processDefName
	 *            流程定义名称
	 * @param userId
	 *            用户ID
	 * @param xpath
	 *            相关参数key
	 * @param relaData
	 *            相关数据value
	 * @return
	 */
	BusinessTransactionBean createProcess(String processDefName, String userId, String xpath, Object relaData);

	/**
	 * 【调用集成】启动流程
	 * 
	 * @param processDefName
	 *            流程定义名称
	 * @param userId
	 *            用户ID
	 * @param isFinishFirst
	 *            是否完成第一节点（对于存在上报节点时，应设置为true）
	 * @return
	 */
	public BusinessTransactionBean createProcess(String processDefName, String userId, boolean isFinishFirst);

	/**
	 * 
	 * 【调用集成】完成工作项
	 * 
	 * @param processInstId
	 * @param workItemId
	 *            活动工作项ID， -1:自动获取
	 * @param xpath
	 *            相关参数key
	 * @param relaData
	 *            相关数据value
	 * @param userId
	 *            用户ID
	 * @return BpsResVo
	 */
	public BusinessTransactionBean finishWorkItem(long processInstId, long workItemId, String xpath, Object relaData,
			String userId);

	/**
	 * 【调用集成】启动流程并完成第一个节点(处理第一个节点的后面是路由节点的情况)
	 * <p>
	 * relaDataMaps是存相关参数参数，map的key是xpath，value是relaData，最多两个map
	 * </p>
	 * 
	 * @param processDefName
	 *            流程定义名称
	 * @param userId
	 *            用户ID
	 * @param relaDataMaps
	 *            相关参数
	 * @return
	 */
	BusinessTransactionBean createProcess(String processDefName, String userId, List<Map<String, Object>> relaDataMaps);

	/**
	 * 创建指定的活动节点，并当前节点跳至该节点
	 * <p>
	 * 用于已有流程实例的情况
	 * </p>
	 * 
	 * @param processInstId
	 *            流程实例ID
	 * @param userId
	 *            跳转前的当前节点的负责用户ID
	 * @param activityDefID
	 *            指定节点的活动定义ID
	 * @return
	 */
	ActivityInstEntity createAndGotoSpecifiedActivity(long processInstId, String userId, String activityDefID);

	/**
	 * 【子流程】创建指定的活动节点，并当前节点跳至该节点
	 * 
	 * @param processInstId
	 *            流程实例
	 * @param userId
	 *            用户ID
	 * @param activityDefID
	 *            指定主流程节点的活动定义ID
	 * @param subActDefID
	 *            指定子流程节点的活动定义ID
	 * @return
	 */
	ActivityInstEntity createAndGotoSpecifiedSubActivity(long processInstId, String userId, String activityDefID,
			String subActDefID);

	/**
	 * 创建指定的活动节点，并当前节点跳至该节点
	 * <p>
	 * 用于还没有流程实例的情况
	 * </p>
	 * <p>
	 * 以前用tiger用户的，传null即可
	 * </p>
	 * 
	 * @param processDefName
	 *            流程定义名称
	 * @param userId
	 *            用户ID
	 * @param activityDefID
	 *            指定节点的活动定义ID
	 * @return
	 */
	WorkItemBean createAndGotoSpecifiedActivity(String processDefName, String userId, String activityDefID);

	/**
	 * 根据流程实例Id获取活动工作项
	 * 
	 * @param processInstId
	 *            活动实例ID
	 * @param userId
	 *            用户ID
	 * @return
	 */
	WorkItemBean getTodoTaskByProcessInstId(Long processInstId, String userId);

}
