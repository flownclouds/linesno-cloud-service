package com.alinesno.cloud.demo.business.repository;
import com.alinesno.cloud.common.core.orm.repository.IBaseJpaRepository;
import com.alinesno.cloud.demo.business.entity.CustomerEntity;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface CustomerRepository extends IBaseJpaRepository<CustomerEntity, String> {

}