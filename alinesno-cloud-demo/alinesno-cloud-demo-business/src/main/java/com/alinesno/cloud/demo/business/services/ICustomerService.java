package com.alinesno.cloud.demo.business.services;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.common.core.services.IBaseService;
import com.alinesno.cloud.demo.business.entity.CustomerEntity;
import com.alinesno.cloud.demo.business.repository.CustomerRepository;

@NoRepositoryBean
public interface ICustomerService extends IBaseService<CustomerRepository, CustomerEntity, String> {

}
