package com.alinesno.cloud.platform.stack.wechat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alinesno.cloud.operation.cmdb.third.wechat.WechatConfig;

import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WechatConfigTest {

	private final Logger logger = LoggerFactory.getLogger(this.getClass()) ; 

	@Autowired
	private WechatConfig wechatConfig ; 
	
	@Test
	public void testSendTemplate() throws WxErrorException {
		WxMpTemplateMessage tip = wechatConfig.sendTemplate("" , "", null);  
		logger.info("tip = {}" , tip);
	}

}
