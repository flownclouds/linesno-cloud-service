package com.alinesno.cloud.operation.cmdb.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 参数实体对象 
 * @author LuoAnDong
 * @since 2018年8月13日 上午7:13:18
 */
@SuppressWarnings("serial")
@Entity
@Table(name="cmdb_params")
public class ParamsEntity extends BaseEntity {

	private String paramName  ;  //参数名称
	private String paramValue ; //参数值 
	private String paramDesc ; //参数描述
	private String paramType ; //参数类型
	private String paramTypeName ; //参数类型名称 
	private String startTime ; //开始时间 
	private String endTime ; //结束时间 
	
	@Column
	private int useParam ; //是否使用(0使用|1禁用),默认 为使用
	
	public String getParamDesc() {
		return paramDesc;
	}
	public void setParamDesc(String paramDesc) {
		this.paramDesc = paramDesc;
	}
	public String getParamName() {
		return paramName;
	}
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}
	public String getParamValue() {
		return paramValue;
	}
	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}
	public String getParamType() {
		return paramType;
	}
	public void setParamType(String paramType) {
		this.paramType = paramType;
	}
	public String getParamTypeName() {
		return paramTypeName;
	}
	public void setParamTypeName(String paramTypeName) {
		this.paramTypeName = paramTypeName;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public int getUseParam() {
		return useParam;
	}
	public void setUseParam(int useParam) {
		this.useParam = useParam;
	}
	
}
