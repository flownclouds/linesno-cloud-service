package com.alinesno.cloud.operation.cmdb.common.constants;

/**
 * 参数列表
 * @author LuoAnDong
 * @since 2018年8月13日 上午7:32:47
 */
public enum ParamsEnum {
	
	RUNNING_TIME("running_time" , "运营时间段") , 
	ORDER_OUTTIME("order_outtime" , "订单超时时间") , 
	ORDER_MIN_PAY("order_min_pay" , "订单最小费用") , 
	ORDER_SALA_OUTTIME("order_sala_outtime" , "人员订单超时时间"),
	RUNNING_SHOP_TIME("running_shop_time" , "超市时间段") , 
	RUNNING_SHOP_MIN_PAY("running_shop_min_pay" , "超市下单最小费用") ,
	WECHAT_SUBSCRIBE("wechat_subscribe" , "微信关注回复信息") ,
	WECHAT_MENUS("wechat_menus" , "微信菜单") ,
	WECHAT_MESSAGE_TEXT("wechat_message_text" , "微信消息自动返回")  , 
	USER_APPLY_TYPE("user_apply_type" , "用户申请类型(simple/complex)") ,
	USER_APPLY_DESC("user_apply_desc" , "用户申请说明") ; 
	
	private String code ; 
	private String text ; 
	
	ParamsEnum(String code , String text){
		this.code = code ; 
		this.text = text ; 
	}

	public String getCode() {
		return code;
	}

	public String getText() {
		return text;
	}
	
}
