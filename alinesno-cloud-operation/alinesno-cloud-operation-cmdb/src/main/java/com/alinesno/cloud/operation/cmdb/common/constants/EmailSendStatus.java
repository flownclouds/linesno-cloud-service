package com.alinesno.cloud.operation.cmdb.common.constants;

/**
 * 写邮件状态 
 */
public enum EmailSendStatus {

	SEND(0) , //待发送
	NO_SEND(1) ; //不再发送
	
	private int value ; 
	
	EmailSendStatus(int v) {
		this.value = v ; 
	}

	public int getValue() {
		return value;
	}
	
}
