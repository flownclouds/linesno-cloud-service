package com.alinesno.cloud.operation.cmdb.third.email;

import java.util.List;

import com.alinesno.cloud.operation.cmdb.common.constants.EmailSendStatus;
import com.alinesno.cloud.operation.cmdb.entity.EmailEntity;
import com.alinesno.cloud.operation.cmdb.web.bean.ResponseBean;

/**
 * 邮件服务
 * @author LuoAnDong
 * @since 2018年8月27日 上午7:54:17
 */
public interface EmailService {

	/**
	 * 发送一封邮件
	 * @param email
	 * @return
	 */
	public ResponseBean<String> sendSingleEmail(String email , String subject , String htmlBody) ; 
	
	/**
	 * 批量发送邮件
	 * @param email 小于50封
	 * @param subject
	 * @param htmlBody
	 * @return
	 */
	public ResponseBean<String> sendSingleEmail(String email[] , String subject , String htmlBody) ;

	/**
	 * 查询邮件列表
	 * @param maxSendSize
	 * @return
	 */
	public List<EmailEntity> findLimit(int maxSendSize);

	/**
	 * 更新邮件信息
	 * @param e
	 */
	public void update(EmailEntity e);

	/**
	 * 当天发送量
	 * @return
	 */
	public int findDaySend(EmailSendStatus emailSendStatus); 
	
}
