package com.alinesno.cloud.operation.cmdb.common.constants;

/**
 * 订单状态
 * 
 * @author LuoAnDong
 * @since 2018年8月5日 下午6:41:23
 */
public enum AccountRoleEnum {
	TENANT("1" , "租房权限") , 
	ADMIN("9" , "超级管理员") , 
	MEMBER("0" , "普通后台管理权限")  ; 

	private String code;
	private String text ;
	
	AccountRoleEnum(String code , String text) {
		this.code = code;
		this.text = text ; 
	}
	
	public static AccountRoleEnum getStatus(String code) {
		if("1".equals(code)) {
			return AccountRoleEnum.TENANT; 
		}else if("0".equals(code)) {
			return AccountRoleEnum.MEMBER; 
		}else if("9".equals(code)) {
			return AccountRoleEnum.ADMIN ; 
		}
		return MEMBER ; 
	}
	
	public String getText() {
		return this.text ; 
	}

	public String getCode() {
		return this.code;
	}
}