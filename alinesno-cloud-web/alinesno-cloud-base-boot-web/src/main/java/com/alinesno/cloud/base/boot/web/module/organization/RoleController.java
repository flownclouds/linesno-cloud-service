package com.alinesno.cloud.base.boot.web.module.organization;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.feign.dto.ManagerResourceDto;
import com.alinesno.cloud.base.boot.feign.dto.ManagerRoleDto;
import com.alinesno.cloud.base.boot.feign.dto.ManagerRoleResourceDto;
import com.alinesno.cloud.base.boot.feign.facade.ManagerResourceFeigin;
import com.alinesno.cloud.base.boot.feign.facade.ManagerRoleFeigin;
import com.alinesno.cloud.base.boot.feign.facade.ManagerRoleResourceFeigin;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.bean.DatatablesPageBean;
import com.alinesno.cloud.common.web.base.controller.FeignMethodController;
import com.alinesno.cloud.common.web.base.form.FormToken;
import com.alinesno.cloud.common.web.base.response.ResponseBean;
import com.alinesno.cloud.common.web.base.response.ResponseGenerator;

/**
 * 部门管理 
 * @author LuoAnDong
 * @since 2019年3月24日 下午1:16:44
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("boot/platform/role")
public class RoleController extends FeignMethodController<ManagerRoleDto , ManagerRoleFeigin> {

	private static final Logger log = LoggerFactory.getLogger(RoleController.class);

	@Autowired
	private ManagerRoleFeigin managerRoleFeigin ; 
	
	@Autowired
	private ManagerResourceFeigin managerResourceFeigin ; 
	
	@Autowired
	private ManagerRoleResourceFeigin managerRoleResourceFeigin ; 
	
	@RequestMapping("/list")
    public void list(){
    }

	/**
	 * 查询出菜单列表
	 * @param roleId
	 * @return 
	 */
	@ResponseBody
	@PostMapping("/menuData")
    public List<ManagerResourceDto> menus(String roleId){
		RestWrapper restWrapper = new RestWrapper() ; 
		restWrapper.eq("roleId", roleId) ; 
		List<ManagerRoleResourceDto> mrList = managerRoleResourceFeigin.findAll(restWrapper) ; 
		
		for(ManagerRoleResourceDto r : mrList) {
			log.debug("r = {}" , r);
		}
		
		if(mrList != null) {
			List<String> resourceIds = new ArrayList<String>() ; 
			for(ManagerRoleResourceDto r : mrList) {
				resourceIds.add(r.getResourceId()) ; 
			}
			List<ManagerResourceDto> list = managerResourceFeigin.findAllById(resourceIds) ; 
			return list ; 
		}
		return null ; 
    }
	
	/**
	 * 查询所有列表
	 * @param roleId
	 * @return 
	 */
	@ResponseBody
	@PostMapping("/allRole")
    public List<ManagerRoleDto> all(String roleId){
		return managerRoleFeigin.findAll() ; 
    }
	
	/**
	 * 保存新对象
	 * 
	 * @param model
	 * @param managerRoleDto
	 * @return
	 */
	@FormToken(remove = true)
	@ResponseBody
	@PostMapping("/saveFunctions")
	public ResponseBean saveFunctions(Model model, HttpServletRequest request, ManagerRoleDto managerRoleDto , String functionIds) {
		if(StringUtils.isBlank(functionIds)) {
			return ResponseGenerator.genFailMessage("请选择功能."); 
		}
		
		boolean isSave = managerRoleFeigin.saveRole(managerRoleDto , functionIds);
		return ResponseGenerator.ok(isSave);
	}

	/**
	 * 代码管理查询功能
	 * 
	 * @return
	 */
	@FormToken(save = true)
	@GetMapping("/add")
	public void add(Model model, HttpServletRequest request) {
	}

	/**
	 * 代码管理查询功能
	 * 
	 * @return
	 */
//	@FormToken(save = true)
//	@GetMapping("/modify")
	@Override
	public void modify(HttpServletRequest request ,  Model model, String id) {
		Assert.hasLength(id, "主键不能为空.");
		ManagerRoleDto code = managerRoleFeigin.getOne(id);
		model.addAttribute("bean", code);
	}

	/**
	 * 删除
	 */
	@ResponseBody
	@PostMapping("/delete")
	public ResponseBean delete(@RequestParam(value = "rowsId[]") String[] rowsId) {
		log.debug("rowsId = {}", ToStringBuilder.reflectionToString(rowsId));
		if (rowsId != null && rowsId.length > 0) {
			managerRoleFeigin.deleteByIds(rowsId);
		}
		return ResponseGenerator.ok(null);
	}

	@TranslateCode("[{hasStatus:has_status}]")
	@ResponseBody
	@RequestMapping("/datatables")
	public DatatablesPageBean datatables(HttpServletRequest request, Model model, DatatablesPageBean page) {
		log.debug("page = {}", ToStringBuilder.reflectionToString(page));
		return this.toPage(model, managerRoleFeigin, page);
	}

//	private DatatablesPageBean toPage(Model model, ManagerRoleFeigin managerRoleFeigin, DatatablesPageBean page) {
//
//		RestWrapper restWrapper = new RestWrapper();
//		RestPage<ManagerRoleDto> pageable = new RestPage<ManagerRoleDto>(page.getStart() / page.getLength(),page.getLength());
//		restWrapper.setPageable(pageable);
//		restWrapper.builderCondition(page.getCondition());
//
//		RestPage<ManagerRoleDto> pageableResult = managerRoleFeigin.findAllByWrapperAndPageable(restWrapper);
//
//		DatatablesPageBean p = new DatatablesPageBean();
//		p.setData(pageableResult.getContent());
//		p.setDraw(page.getDraw());
//		p.setRecordsFiltered((int) pageableResult.getTotalElements());
//		p.setRecordsTotal((int) pageableResult.getTotalElements());
//
//		return p;
//	}
	
	/**
	 * 保存新对象
	 * 
	 * @param model
	 * @param managerRoleDto
	 * @return
	 */
	@FormToken(remove = true)
	@ResponseBody
	@PostMapping("/updateFunctions")
	public ResponseBean updateFunctions(Model model, HttpServletRequest request, ManagerRoleDto managerRoleDto , String functionIds) {
		return this.saveFunctions(model, request, managerRoleDto, functionIds) ; 
	}


}
