package com.alinesno.cloud.portal.desktop.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-05-20 21:26:06
 */
@Entity
@Table(name="link_path_type")
public class LinkPathTypeEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 类型名称
     */
	@Column(name="link_type_name")
	private String linkTypeName;
    /**
     * 类型代码
     */
	@Column(name="link_type_code")
	private String linkTypeCode;
    /**
     * 链接分组
     */
	@Column(name="link_group")
	private String linkGroup;


	public String getLinkTypeName() {
		return linkTypeName;
	}

	public void setLinkTypeName(String linkTypeName) {
		this.linkTypeName = linkTypeName;
	}

	public String getLinkTypeCode() {
		return linkTypeCode;
	}

	public void setLinkTypeCode(String linkTypeCode) {
		this.linkTypeCode = linkTypeCode;
	}

	public String getLinkGroup() {
		return linkGroup;
	}

	public void setLinkGroup(String linkGroup) {
		this.linkGroup = linkGroup;
	}


	@Override
	public String toString() {
		return "LinkPathTypeEntity{" +
			"linkTypeName=" + linkTypeName +
			", linkTypeCode=" + linkTypeCode +
			", linkGroup=" + linkGroup +
			"}";
	}
}
