package com.alinesno.cloud.portal.desktop.web.repository;

import java.util.List;

import com.alinesno.cloud.common.core.orm.repository.IBaseJpaRepository;
import com.alinesno.cloud.portal.desktop.web.entity.LinkPathEntity;

/**
 * <p>
  *  持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-05-18 14:46:04
 */
public interface LinkPathRepository extends IBaseJpaRepository<LinkPathEntity, String> {

	List<LinkPathEntity> findAllByLinkDesign(String type);

	List<LinkPathEntity> findAllByLinkType(String linkTypeCode);

}
