package com.alinesno.cloud.portal.desktop.web.bean;

import java.util.List;

/**
 * 学习实体
 * @author LuoAnDong
 * @since 2019年5月14日 上午7:19:40
 */
public class TrainingLearnBean {

	private String id ; 
	private String icons ; 
	private String header ; 
	private String desc ; 
	private int status ; 
	private String href ;
	
	private List<TrainingLearnBean> list ; 
	
	public TrainingLearnBean() {
		super();
	}

	public TrainingLearnBean(String icons, String header, List<TrainingLearnBean> list) {
		super();
		this.icons = icons;
		this.header = header;
		this.list = list;
	}

	public TrainingLearnBean(String icons, String header, String href , String desc , int status) {
		this.icons = icons;
		this.header = header;
		this.desc = desc;
		this.href = href;
		this.status = status ; 
	}
	
	public List<TrainingLearnBean> getList() {
		return list;
	}

	public void setList(List<TrainingLearnBean> list) {
		this.list = list;
	}

	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIcons() {
		return icons;
	}

	public void setIcons(String icons) {
		this.icons = icons;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	} 
	
}
