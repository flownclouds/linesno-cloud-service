package com.alinesno.cloud.portal.desktop.web.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;
import com.alinesno.cloud.portal.desktop.web.entity.LinkPathTypeEntity;
import com.alinesno.cloud.portal.desktop.web.repository.LinkPathTypeRepository;
import com.alinesno.cloud.portal.desktop.web.service.ILinkPathTypeService;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-05-18 14:46:04
 */
@Service
public class LinkPathTypeServiceImpl extends IBaseServiceImpl<LinkPathTypeRepository, LinkPathTypeEntity, String> implements ILinkPathTypeService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(LinkPathTypeServiceImpl.class);

}
