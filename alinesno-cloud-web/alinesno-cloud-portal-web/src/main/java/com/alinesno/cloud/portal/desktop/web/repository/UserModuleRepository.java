package com.alinesno.cloud.portal.desktop.web.repository;

import java.util.List;

import com.alinesno.cloud.common.core.orm.repository.IBaseJpaRepository;
import com.alinesno.cloud.portal.desktop.web.entity.UserModuleEntity;

/**
 * <p>
  *  持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-21 19:10:44
 */
public interface UserModuleRepository extends IBaseJpaRepository<UserModuleEntity, String> {

	void deleteByUid(String id);

	List<UserModuleEntity> findAllByUid(String id);
	
}
