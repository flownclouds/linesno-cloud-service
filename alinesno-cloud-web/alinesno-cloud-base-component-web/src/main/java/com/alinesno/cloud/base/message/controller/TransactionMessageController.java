package com.alinesno.cloud.base.message.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.message.feign.dto.TransactionMessageDto ;
import com.alinesno.cloud.base.message.feign.facade.TransactionMessageFeigin ;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.bean.DatatablesPageBean;
import com.alinesno.cloud.common.web.base.controller.FeignMethodController;

/**
 * <p> 前端控制器 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-08 09:28:52
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("base/message/transactionMessage")
public class TransactionMessageController extends FeignMethodController<TransactionMessageDto, TransactionMessageFeigin> {

	//日志记录
	private static final Logger log = LoggerFactory.getLogger(TransactionMessageController.class);

	@TranslateCode(value="[{hasStatus:has_status}]")
	@ResponseBody
	@PostMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
		return this.toPage(model, feign , page) ;
    }

	/**
	 * 查看监控 
	 * @param applicationId
	 * @return
	 */
	@RequestMapping("/monitor")
	public void monitor() {
		log.debug("监控页面.");
	}
	
}


























