package com.alinesno.cloud.common.web.base.thymeleaf;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Component;
import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.processor.IProcessor;
import org.thymeleaf.standard.processor.StandardXmlNsTagProcessor;
import org.thymeleaf.templatemode.TemplateMode;

/**
 * 自定义标签
 * @author LuoAnDong
 * @since 2019年1月31日 下午3:24:56
 */
@Component
public class TagsDialect extends AbstractProcessorDialect {

	private static final String TAG_PREFIX = "self" ; 

	public TagsDialect() {
		super("self tags", TAG_PREFIX , 1000);
	}

	/**
	 * 添加自定义标签解析
	 */
	@Override
	public Set<IProcessor> getProcessors(final String dialectPrefix) {
		final Set<IProcessor> processors = new HashSet<>();
		
        //添加我们定义的标签_start
        processors.add(new DictionaryTagProcessor(dialectPrefix)); // 代码标签
        //添加我们定义的标签_end 
        
		processors.add(new StandardXmlNsTagProcessor(TemplateMode.HTML, dialectPrefix));
		return processors;
	}
}